import math
import matplotlib.pyplot as plt
import numpy as np
import sortedcontainers as sc
import functions as fs


def set_con(a, b):
    if a == b:
        return -1
    return 0


def find_first_closest_dots(mt):
    d1, d2, min_dist = 0, 0, 2 * radius
    for w1 in range(dot_count):
        for w2 in range(w1 + 1, dot_count):
            if mt[w1][w2][0] < min_dist:
                d1, d2, min_dist = w1, w2, mt[w1][w2][0]
    mt[d1][d2][1] = 1
    return d1, d2


def find_closest_dot(mt, bounded_dots):
    d1, d2, min_dist = 0, 0, 2 * radius
    is_first = bool(0)
    for bounded_dot in bounded_dots:
        for w1 in range(bounded_dot + 1, dot_count):
            if (mt[bounded_dot][w1][0] < min_dist) & (mt[bounded_dot][w1][1] == 0) & (cl[w1] == 0):
                d1, d2 = bounded_dot, w1
                min_dist = mt[bounded_dot][w1][0]
                is_first = 0
        for w1 in range(bounded_dot):
            if (mt[w1][bounded_dot][0] < min_dist) & (mt[w1][bounded_dot][1] == 0) & (cl[w1] == 0):
                d1, d2 = w1, bounded_dot
                min_dist = mt[w1][bounded_dot][0]
                is_first = 1

    mt[d1][d2][1] = 1
    return d1, d2, is_first


def get_dot(d1, d2, is_first):
    if is_first:
        return d1
    return d2


def clear_line(dt1, dt2, is_manual=1):
    dtx = [dt1[0], dt2[0]]
    dty = [dt1[1], dt2[1]]
    if is_manual:
        plt.waitforbuttonpress()
    else:
        plt.pause(pause_time)
    plt.plot(dtx, dty, c='w', linewidth=1)
    plt.draw()
    return 0


def dispose_clusters(is_final, is_manual=0):
    for cluster_ind in range(k + 1):
        dt = fs.in_this_cluster(arr, cluster_ind, dot_count, cl)
        plt.scatter(dt[0], dt[1], c=colors[cluster_ind], s=dot_size)
    if is_final:
        plt.show()
        return 0
    if is_manual:
        plt.waitforbuttonpress()
    else:
        plt.pause(pause_time)
    plt.draw()
    return 0


def define_graph(dot_ind, cluster_ind):
    cl[dot_ind] = cluster_ind
    for w1 in range(dot_ind + 1, dot_count):
        if (mat[dot_ind][w1][1] == 1) & (cl[w1] != cluster_ind):
            define_graph(w1, cluster_ind)
    for w1 in range(dot_ind):
        if (mat[w1][dot_ind][1] == 1) & (cl[w1] != cluster_ind):
            define_graph(w1, cluster_ind)
    return 0


def find_and_remove_max(mt):
    max_list = sc.SortedKeyList([[0, 0, 0] for i in range(k - 1)], key=lambda x: x[2])
    for w1 in range(dot_count):
        for w2 in range(w1 + 1, dot_count):
            if (mt[w1][w2][1] != 0) & (mt[w1][w2][0] > max_list[0][2]):
                max_list.add([w1, w2, mt[w1][w2][0]])
                max_list.pop(0)
    cluster_ind = 2
    for i in max_list:
        mt[i[0]][i[1]][1] = 0
    for i in max_list:
        clear_line(arr[i[0]], arr[i[1]])
        if cl[i[1]] == 1:
            define_graph(i[1], cluster_ind)
        else:
            define_graph(i[0], cluster_ind)
        cluster_ind += 1
    return 0


dot_size = 3
dot_count = 25
k = 7  # Cluster count (one for init)
radius = 1.0
pause_time = 0.03
on_click = 0
colors = ['k', 'b', 'g', 'r', 'c', 'm', 'y', 'aqua', 'deepskyblue', 'violet', 'orange']

# arr = np.array([fs.descartes_random_dot() for n in range(dot_count)])
arr = np.array([[radius * math.cos(2 * n * math.pi / dot_count), radius * math.sin(2 * n * math.pi / dot_count)]
                for n in range(dot_count)])
#  shaped like
#  0 1 1 1 1
#  * 0 1 1 1
#  * * 0 1 1
#  * * * 0 1
#  * * * * 0
# mat = np.array([[[fs.euclid_distance(arr[i], arr[j]), set_con(i, j)] for j in range(dot_count)] for i in range(dot_count)])
mat = np.array([[[fs.random_weight(), set_con(i, j)] for j in range(dot_count)] for i in range(dot_count)])
cl = [0 for b in range(dot_count)]  # dot's cluster
dispose_clusters(0, on_click)

dot_ind1, dot_ind2 = find_first_closest_dots(mat)
cl[dot_ind1], cl[dot_ind2] = 1, 1
bounded = sc.SortedSet([dot_ind1, dot_ind2])  # bounded dots
fs.paint_line(arr[dot_ind1], arr[dot_ind2], on_click, 1, pause_time, colors[1], dot_size)

for i in range(dot_count - 2):
    dot_ind1, dot_ind2, is_f = find_closest_dot(mat, bounded)
    temp_dot_ind = get_dot(dot_ind1, dot_ind2, is_f)
    cl[temp_dot_ind] = 1
    bounded.add(temp_dot_ind)
    fs.paint_line(arr[dot_ind1], arr[dot_ind2], on_click, 1, pause_time, colors[1], dot_size)
print("Press a button to remove an interval")
find_and_remove_max(mat)
plt.waitforbuttonpress()
plt.clf()
dispose_clusters(1, 1)
