# https://www.geeksforgeeks.org/understanding-logistic-regression/
import matplotlib.pyplot as plt
import numpy as np


def normalize(x):
    mins = np.min(x, axis=0)
    maxs = np.max(x, axis=0)
    rng = maxs - mins
    norm_x = 1 - ((maxs - x) / rng)
    return norm_x


def sigmoid(b, x):
    return 1.0 / (1 + np.exp(-np.dot(x, b.T)))


def grad_desc(x, y, b, iters=10000, learn_r=.01):
    for i in range(iters):
        b = b - learn_r * np.dot((sigmoid(b, x) - y.reshape(x.shape[0], -1)).T, x)
    return b


def pred_class(b, x):
    pred_prob = sigmoid(b, x)
    pred_value = np.where(pred_prob >= .5, 1, 0)
    return np.squeeze(pred_value)


def paint_plot(x, y, b):
    x_0 = x[np.where(y == 0.0)]
    x_1 = x[np.where(y == 1.0)]
    plt.scatter([x_0[:, 1]], [x_0[:, 2]], c='b', alpha=0.3, s=4)
    plt.scatter([x_1[:, 1]], [x_1[:, 2]], c='r', alpha=0.3, s=4)
    x1 = np.arange(0, 1.01, 0.1)
    x2 = -(b[0, 0] + b[0, 1] * x1) / b[0, 2]
    plt.plot(x1, x2, c='k')
    plt.show()


def show_logistic_regression():
    np.random.seed(10)
    class_dot_count = 3000
    cl1 = np.random.multivariate_normal([0, 0], [[1, 0.5], [0.5, 1]], class_dot_count)
    cl2 = np.random.multivariate_normal([1, 4], [[1, 0.5], [0.5, 1]], class_dot_count)
    xx = normalize(np.vstack((cl1, cl2)))
    xx = np.hstack((np.matrix(np.ones(xx.shape[0])).T, xx))
    yy = np.hstack((np.zeros(class_dot_count), np.ones(class_dot_count)))

    beta = np.matrix(np.zeros(xx.shape[1]))  # init

    beta = grad_desc(xx, yy, beta)
    print("coeff: (%.2f) + (%.2f)x1 + (%.2f)x2 " % (beta[0, 0], beta[0, 1], beta[0, 2]))

    y_pred = pred_class(beta, xx)
    print("Correct predictions' proportion: %.4f" % (np.sum(yy == y_pred) / (2 * class_dot_count)))
    paint_plot(xx, yy, beta)


if __name__ == "__main__":
    show_logistic_regression()
