import math
import kmeans
import matplotlib.pyplot as plt
import functions as fs
import sortedcontainers as sc

tr_set_size, input_dots_count, cl_count, fs.radius, kmeans.dot_size = 36, 10, 4, 1.0, 10
k = round(math.sqrt(tr_set_size))
arr, cl = kmeans.k_means(dot_count=tr_set_size, paint_all=0, gen_func=fs.descartes_random_dot, cl_count=cl_count,
                         radius=fs.radius)
for dot_numb in range(input_dots_count):
    cur_dot = fs.descartes_random_dot()
    print("Insert dot ", cur_dot)
    plt.waitforbuttonpress()
    dists_indexes = sc.SortedKeyList([i for i in range(tr_set_size)],
                                     key=lambda x: fs.euclid_distance(cur_dot, arr[x]))
    cl_neighbor = [[i, 0] for i in range(cl_count)]
    for j in dists_indexes[:k]:
        cl_neighbor[cl[j]][1] += 1
    max_cl = max(cl_neighbor, key=lambda x: x[1])[1]
    cl_neighbor = list(filter(lambda x: x[1] == max_cl, cl_neighbor))
    color = kmeans.colors[cl_neighbor[0][0]]
    if len(cl_neighbor) != 1:
        color = 'k'
        print("Collision! Dot will be painted black. Clusters: ", list(map(lambda x: kmeans.colors[x[0]], cl_neighbor)))
    plt.scatter(cur_dot[0], cur_dot[1], c=color, marker='X', s=25)
    plt.draw()
