import math
import matplotlib.pyplot as plt
import numpy as np
import functions as fs

colors = ['b', 'g', 'r', 'c', 'm', 'y', 'aqua', 'deepskyblue', 'violet', 'orange']
dot_size = 3


def nearest_cluster(d, radius, cluster_count, cluster_dots):
    ind, mn = 0, 2 * radius
    for l in range(cluster_count):
        temp = fs.euclid_distance(d, cluster_dots[l])
        if temp < mn:
            mn = temp
            ind = l
    return ind


def dispose_dots(is_final, arr, cl, k, dot_count, cluster_dots, pause_time, show_cluster_center=1, clear_plot=1):
    for cluster_ind in range(k):
        dt = fs.in_this_cluster(arr, cluster_ind, dot_count, cl)
        plt.scatter(dt[0], dt[1], c=colors[cluster_ind], s=dot_size)
        if show_cluster_center:
            plt.scatter(cluster_dots[cluster_ind][0], cluster_dots[cluster_ind][1], c='k', marker='X', s=30)
    if is_final:
        plt.show()
        return 0
    plt.draw()
    plt.pause(pause_time)
    # plt.waitforbuttonpress()
    if clear_plot:
        plt.clf()
    return 0


def k_means(paint_all=1, arr=None, cl=None, cl_count=7, dot_count=10000, radius=1.0, pause_time=0.3,
            gen_func=fs.random_dot):
    if cl is None:
        cl = [0 for b in range(dot_count)]  # dot's cluster
    if arr is None:
        arr = np.array([gen_func() for n in range(dot_count)])  # Main array
    cluster_dots = []  # Clusters centers
    for i in range(cl_count):
        cluster_dots.append(
            [radius * math.cos(2 * i * math.pi / cl_count), radius * math.sin(2 * i * math.pi / cl_count)])
    iter_count = 0
    f = bool(1)
    while f == bool(1):
        iter_count += 1
        temp_f = bool(1)
        for i in range(dot_count):
            tem_d = nearest_cluster(arr[i], radius, cl_count, cluster_dots)
            if cl[i] != tem_d:
                temp_f = bool(0)
            cl[i] = tem_d
        d_list = [{'sumX': .0, 'sumY': .0, 'count': 0} for n in range(cl_count)]
        for i in range(dot_count):
            t_cl = d_list[cl[i]]
            t_cl['sumX'] += arr[i][0]
            t_cl['sumY'] += arr[i][1]
            t_cl['count'] += 1
        for j in range(cl_count):
            t_cl = d_list[j]
            if t_cl != 0:
                cluster_dots[j] = [t_cl['sumX'] / t_cl['count'], t_cl['sumY'] / t_cl['count']]
        if paint_all:
            dispose_dots(bool(0), arr, cl, cl_count, dot_count, cluster_dots, pause_time)
        if temp_f:
            break
    print("Iteration's count", iter_count)
    dispose_dots(paint_all, arr, cl, cl_count, dot_count, cluster_dots, pause_time, paint_all, 0)
    return arr, cl
