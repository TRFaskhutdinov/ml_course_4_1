import math
import random as rnd
from functools import reduce
import matplotlib.pyplot as plt
import numpy as np

radius = 1.0
rnd.seed()


def random_dot():
    fi = rnd.vonmisesvariate(0, 0)
    ran = rnd.random()
    qq = (ran * radius * math.cos(fi), ran * radius * math.sin(fi))
    return qq


def random_dot_fixed_radius():
    fi = rnd.vonmisesvariate(0, 0)
    qq = (radius * math.cos(fi), radius * math.sin(fi))
    return qq


def random_weight():
    return rnd.random()


def descartes_random_dot():
    qq = (rnd.uniform(-radius, radius), rnd.uniform(-radius, radius))
    return qq


def euclid_distance(a, b):
    return math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]))


def in_this_cluster(ar, cluster_ind, dot_count, cl):
    ret_x, ret_y = [], []
    for dot_ind in range(dot_count):
        if cl[dot_ind] == cluster_ind:
            ret_x.append(ar[dot_ind][0])
            ret_y.append(ar[dot_ind][1])
    return [np.array(ret_x), np.array(ret_y)]


def scalar_prod(a, b):
    return reduce(lambda x, y: x + y[0] * y[1], zip(a, b), 0)


def paint_line(dt1, dt2, is_manual=0, visible_dots=1, pause_time=0.03, color='violet', dot_size=3):
    if is_manual:
        plt.waitforbuttonpress()
    else:
        plt.pause(pause_time)
    dtx = [dt1[0], dt2[0]]
    dty = [dt1[1], dt2[1]]
    if visible_dots:
        plt.scatter(dtx, dty, c=color, s=dot_size)
    plt.plot(dtx, dty, c=color, linewidth=1)
    plt.draw()
    return 0
