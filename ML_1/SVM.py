# https://hsto.org/getpro/habr/post_images/509/2cc/1d2/5092cc1d293d5e85d253339c314d37af.png
import copy
import math
from functools import reduce

import matplotlib.pyplot as plt
import numpy as np
import functions as fs
import kmeans

# http://www.machinelearning.ru/wiki/index.php?title=SVM
cluster_count, dot_count, fs.radius = 3, 30, 0.6
shift = [(0.866, 0.5), (-0.866, 0.5), (0, -1)]
show_training_dots, plot_size = 0, 2


def get_c_ind(ind1, ind2, dot_new_count):
    return int((dot_new_count - 2 - ind1 / 2) * (ind1 + 1) + (ind2 - ind1) - 1)


def compute_eq(lamb, dots):  # compute line's parameters
    ww1 = list(map(lambda x: x[1], filter(lambda x1: x1[0] != 0, zip(lamb, dots))))
    w = reduce(lambda x, y: x + y[0] * y[1] * y[2], zip(lamb, map(lambda x: x[-1], dots),
                                                        np.array(list(map(lambda x: x[:-1], dots)))), 0)
    b = reduce(lambda x, y: x + (y[-1] - fs.scalar_prod(w, y[:-1])), ww1, 0) / len(ww1)
    if show_training_dots:
        plt.scatter(list(map(lambda x: x[0], ww1)), list(map(lambda x: x[1], ww1)), c='orange', s=10)
    return w, b


def solve_f(dot1, dot2, st1s, st2s, st1c):  # find critical dots of Lagrangian function (for 2 not-null dots)
    if dot1[-2] > dot2[-2]:
        dot1, dot2 = dot2, dot1
    lmbd1 = st1s[dot1[-2]][-1] + st1s[dot2[-2]][-1]
    comb = list(filter(lambda x: (x[0] == dot1[-2]) and (x[1] == dot2[-2]), st1c))[0]
    lmbd2 = st2s[dot1[-2]][-1] + st2s[dot2[-2]][-1] + comb[-1]
    lmbd = - lmbd1 / (2 * lmbd2)
    return (lmbd2 * lmbd + lmbd1) * lmbd, lmbd, lmbd


def comp_y(res, x):
    if res[0][1] == 0:
        return [-res[-1]/res[0][0], -x], [-res[-1]/res[0][0], x]
    else:
        y1, y2 = -(res[-1] - res[0][0] * x) / res[0][1], -(res[-1] + res[0][0] * x) / res[0][1]
        x1, x2 = -x, x
        if abs(y1) > abs(x):
            y1 = x * y1 / abs(y1)
            x1 = - (y1 * res[0][1] + res[-1]) / res[0][0]
        if abs(y2) > abs(x):
            y2 = x * y2 / abs(y2)
            x2 = - (y2 * res[0][1] + res[-1]) / res[0][0]
        return [x1, y1], [x2, y2]


def compare(r, s1, s2, i):  # get lines and clip excessive line's intervals
    dot1, dot2 = comp_y(r, plot_size)
    if i == 0:
        return max(dot1, dot2, key=lambda x: x[1]), min(s1, s2, key=lambda x: x[1])
    if i == 1:
        return min(dot1, dot2, key=lambda x: x[1]), max(s1, s2, key=lambda x: x[1])
    if i == 2:
        return min(dot1, dot2, key=lambda x: x[1]), max(s1, s2, key=lambda x: x[1])


def comp_s_vect(wh_arr, dot_cluster, cl_numb1, cl_numb2):  # compute line between classes
    div_dot = []
    for k1, ww in enumerate(wh_arr):
        if dot_cluster[k1] == cl_numb1:
            div_dot.append((ww[0], ww[1], 1))
        if dot_cluster[k1] == cl_numb2:
            div_dot.append((ww[0], ww[1], -1))
    cnt = len(div_dot)
    st_1_single = [[i, 1] for i in range(cnt)]  # a1*lambda1, a2*lambda2 ...
    st_2_single = [[i, - fs.scalar_prod(div_dot[i][:-1], div_dot[i][:-1]) / 2] for i in range(cnt)]  # a1*lmd1^2 ..
    st_1_comb = [[i, j, - fs.scalar_prod(div_dot[i][:-1], div_dot[j][:-1]) * div_dot[i][-1] * div_dot[j][-1]]
                 for i in range(cnt) for j in range(i + 1, cnt)]  # a1*lambda1*lambda2 ...
    arch_1, arch_2, arch_3 = copy.deepcopy(st_1_single), copy.deepcopy(st_2_single), copy.deepcopy(st_1_comb)
    # dont divide by 2, cause have not doubled combinations
    cond = list(map(lambda x: x[2] * (-1) * div_dot[-1][2], div_dot[:-1]))  # lambda sum = 0
    last_1_single = st_1_single[-1]
    last_2_single = st_2_single[-1]
    st_1_single = list(
        map(lambda x: [x[0][0], x[0][1] + x[1] * last_1_single[1]], zip(st_1_single, cond)))  # from lin component (1)
    st_2_single = list(map(lambda x: [x[0][0], x[0][1] + x[1] * x[1] * last_2_single[1]],
                           zip(st_2_single, cond)))  # from quad component (4)
    st_1_comb_ins = list(filter(lambda x: x[1] == last_1_single[0], st_1_comb))  # replaced comb components
    st_1_comb = list(filter(lambda x: x[1] != last_1_single[0], st_1_comb))  # rest
    temp_ind = 0
    for i1 in range(cnt - 1):  # from quad component (3)
        for i2 in range(i1 + 1, cnt - 1):
            st_1_comb[temp_ind][2] += 2 * cond[i1] * cond[i2] * last_2_single[-1]
            temp_ind += 1
    for i1 in range(cnt - 1):  # from comb component (2)
        for i2 in range(cnt - 1):
            if i1 == i2:  # to quad comp
                st_2_single[i1][1] += st_1_comb_ins[i1][-1] * cond[i2]
            else:  # to comb component
                st_1_comb[get_c_ind(min(i1, i2), max(i1, i2), cnt - 1)][-1] += st_1_comb_ins[i1][-1] * cond[i2]

    matrix = [[0 for i in range(cnt - 1)] for j in range(cnt - 1)]
    matrix_b = []
    for lmbd in range(cnt - 1):
        st_2_single_diff = st_2_single[lmbd]
        st_1_comb_diff_first = list(filter(lambda x: x[0] == lmbd, st_1_comb))
        st_1_comb_diff_last = list(filter(lambda x: x[1] == lmbd, st_1_comb))
        matrix[lmbd][lmbd] += 2 * st_2_single_diff[-1]
        for i1 in st_1_comb_diff_first:
            matrix[lmbd][i1[1]] += i1[2]
        for i1 in st_1_comb_diff_last:
            matrix[lmbd][i1[0]] += i1[2]
        matrix_b.append(- st_1_single[lmbd][-1])
    res = np.linalg.solve(matrix, matrix_b)
    res = list(res) + [reduce(lambda x, y: x + y[0] * y[1], zip(res, cond), 0)]
    if all(map(lambda x: x >= 0, res)):
        return compute_eq(res, div_dot)
    else:  # if not all lambda >= 0, try to maximize by selection
        dot_cl1, dot_cl2 = [], []
        for ind, dt in enumerate(div_dot):
            if dt[-1] == 1:
                dot_cl1.append(list(dt[:-1]) + [ind] + [dt[-1]])
            else:
                dot_cl2.append(list(dt[:-1]) + [ind] + [dt[-1]])
        max_f, max_val1, max_val2 = solve_f(dot_cl1[0], dot_cl2[0], arch_1, arch_2, arch_3)
        max_d = (0, 0)
        for ind1, dt1 in enumerate(dot_cl1):
            for ind2, dt2 in enumerate(dot_cl2):
                temp, temp_v1, temp_v2 = solve_f(copy.deepcopy(dt1), copy.deepcopy(dt2), arch_1, arch_2, arch_3)
                if temp > max_f:
                    max_f, max_d, max_val1, max_val2 = temp, (ind1, ind2), temp_v1, temp_v2
        res = [0 for i in range(cnt)]
        res[dot_cl1[max_d[0]][-2]], res[dot_cl2[max_d[1]][-2]] = max_val1, max_val2
        return compute_eq(res, div_dot)


def show_svm():
    arr = np.array([tuple(
        map(sum, zip(fs.random_dot(), shift[math.trunc(i / (dot_count / cluster_count))]))) for i in range(dot_count)])
    dot_cls = [math.trunc(i / (dot_count / cluster_count)) for i in range(dot_count)]
    kmeans.dispose_dots(0, arr, dot_cls, cluster_count, dot_count, [], 0.3, show_cluster_center=0, clear_plot=0)

    r1 = comp_s_vect(arr, dot_cls, 0, 1)
    r2 = comp_s_vect(arr, dot_cls, 1, 2)
    r3 = comp_s_vect(arr, dot_cls, 0, 2)
    s01 = np.linalg.solve(np.array([r1[0], r2[0]]), np.array([-r1[1], -r2[1]]))
    s02 = np.linalg.solve(np.array([r1[0], r3[0]]), np.array([-r1[1], -r3[1]]))
    de1, de2 = compare(r1, s01, s02, 0)
    fs.paint_line(de1, de2, visible_dots=0)
    s12 = np.linalg.solve(np.array([r2[0], r3[0]]), np.array([-r2[1], -r3[1]]))
    de1, de2 = compare(r2, s01, s12, 1)
    fs.paint_line(de1, de2, visible_dots=0)
    de1, de2 = compare(r3, s02, s12, 2)
    fs.paint_line(de1, de2, visible_dots=0)
